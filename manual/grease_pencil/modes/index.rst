
##############################
  Grease Pencil Object Modes
##############################

Object Modes allow editing different aspects of Grease Pencil objects.
These modes are specifically tailored to the Grease Pencil object,
unlike the more general modes, which work for other object types
(with the exception of object mode which is the same for all objects).

.. toctree::
   :maxdepth: 1

   draw/index.rst

Draw Mode is where new strokes are created.
Strokes are directly sketched on a canvas, using different tools and brushes.

.. toctree::
   :maxdepth: 1

   sculpting/index.rst

Sculpt Mode can be used to deform and shape existing strokes more organically.
Strokes can be smoothed, deformed, or reshaped, adding fluidity and dynamism to drawings.

.. toctree::
   :maxdepth: 1

   edit/index.rst

Edit Mode allows modifying individual strokes and points of Grease Pencil objects.
This mode is ideal for fine-tuning linework, adjusting shapes, and refining details.

.. toctree::
   :maxdepth: 1

   vertex_paint/index.rst

Vertex Paint Mode allows adding color the vertices of strokes directly.
This mode is useful for adding shading, gradients, or detailed color effects
providing finer control over the drawing's appearance.

.. toctree::
   :maxdepth: 1

   weight_paint/index.rst

Weight Paint Mode allows assigning vertex weights to strokes.
This is crucial for rigging and animating characters, ensuring smooth,
and precise deformations based on the painted weights.

.. toctree::
   :maxdepth: 1

   object/index.rst

This mode allows working with the entire Grease Pencil object as a whole.
It's used for overall transformations and managing the placement of the object within the scene.
