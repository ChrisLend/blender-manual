
********
Overview
********

There are a number of brushes for draw mode bundled in the *Essentials* asset library. This is an
overview of all of them.

:doc:`Draw <draw>`
   Draw free-hand strokes.

:doc:`Tint <tint>`
   Colorize stroke points.
