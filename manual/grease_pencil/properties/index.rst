.. _bpy.types.GPencilSculptSettings:

##############
  Properties
##############

.. toctree::
   :maxdepth: 2

   object.rst
   data.rst
