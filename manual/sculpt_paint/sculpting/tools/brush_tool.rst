*****
Brush
*****

.. reference::

   :Mode:      Sculpt Mode
   :Tool:      :menuselection:`Toolbar --> Brush`


Tool to use for any of the *Sculpt* mode brushes. Activating a brush from an asset shelf or brush
selector will also activate this tool for convenience.

See :doc:`brushes </sculpt_paint/sculpting/brushes/index>` (or the available
:doc:`Brush Types </sculpt_paint/sculpting/brushes/brush_types>`) for a detailed list of all
brushes and their options.
