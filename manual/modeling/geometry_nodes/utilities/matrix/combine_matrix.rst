.. index:: Geometry Nodes; Combine Matrix
.. _bpy.types.FunctionNodeCombineMatrix:

*******************
Combine Matrix Node
*******************

.. figure:: /images/node-types_FunctionNodeCombineMatrix.webp
   :align: right
   :alt: Combine Matrix node.

The *Combine Matrix* node constructs a 4x4 matrix from its individual values.


Inputs
======

The inputs of this node are split into panels for each column of the matrix.
Each panel, has four value inputs for the four rows of the matrix.


Properties
==========

This node has no properties.


Outputs
=======

Matrix
   The constructed matrix.
