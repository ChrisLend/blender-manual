.. index:: Geometry Nodes; Combine Transform
.. _bpy.types.FunctionNodeCombineTransform:

**********************
Combine Transform Node
**********************

.. figure:: /images/node-types_FunctionNodeCombineTransform.webp
   :align: right
   :alt: Combine Transform node.

The *Combine Transform* node combines a translation vector,
a rotation vector, and a scale vector into a :term:`Transformation Matrix`.


Inputs
======

Translation
   The translation vector.
Rotation
   The rotation vector.
Scale
   The scale vector.


Properties
==========

This node has no properties.


Outputs
=======

Transform
   The combined transformation matrix.
