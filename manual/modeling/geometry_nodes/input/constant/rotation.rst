.. index:: Geometry Nodes; Rotation
.. _bpy.types.FunctionNodeInputRotation:

*************
Rotation Node
*************

.. figure:: /images/node-types_FunctionNodeInputRotation.webp
   :align: right
   :alt: Rotation Node.

The *Rotation* input creates a rotation from euler rotation values.


Inputs
======

This node has no inputs.


Properties
==========

X, Y, Z
   The amount of rotation about each axes.


Output
======

Vector
   Standard rotation output.
