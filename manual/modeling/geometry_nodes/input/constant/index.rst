
########################
  Input Constant Nodes
########################

Nodes used mainly as input to other nodes.

.. toctree::
   :maxdepth: 1

   boolean.rst
   color.rst
   image.rst
   integer.rst
   material.rst
   rotation.rst
   string.rst
   value.rst
   vector.rst
