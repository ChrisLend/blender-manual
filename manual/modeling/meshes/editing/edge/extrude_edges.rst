.. _bpy.ops.mesh.extrude_edges_move:

*************
Extrude Edges
*************

.. reference::

   :Mode:      Edit Mode
   :Menu:      :menuselection:`Edge --> Extrude Edges`
               :menuselection:`Mesh --> Extrude --> Extrude Edges`
   :Shortcut:  :kbd:`E`

Extrude edges as individual edges.

.. list-table::

   * - .. figure:: /images/modeling_meshes_editing_edge_extrude-edges_before.png
          :width: 320px

          Edge selected.

     - .. figure:: /images/modeling_meshes_editing_edge_extrude-edges_after.png
          :width: 320px

          Edge extruded.
